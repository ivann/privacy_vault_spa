defmodule PrivacyVault.Repo do
  use Ecto.Repo,
    otp_app: :privacy_vault,
    adapter: Ecto.Adapters.Postgres
end
