defmodule PrivacyVault.Vault.Request do
  use Ecto.Schema
  import Ecto.Changeset


  schema "requests" do
    field :center, :string
    field :email, :string
    field :info, :string
    field :key, :string

    timestamps()
  end

  @doc false
  def changeset(request, attrs) do
    request
    |> cast(attrs, [:email, :key, :info, :center])
    |> validate_required([:email, :key, :info, :center])
  end
end
