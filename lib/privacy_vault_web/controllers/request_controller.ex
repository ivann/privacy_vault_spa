defmodule PrivacyVaultWeb.RequestController do
  use PrivacyVaultWeb, :controller

  alias PrivacyVault.Vault
  alias PrivacyVault.Vault.Request

  action_fallback PrivacyVaultWeb.FallbackController

  def index(conn, _params) do
    requests = Vault.list_requests()
    render(conn, "index.json", requests: requests)
  end

  def create(conn, %{"request" => request_params}) do
    with {:ok, %Request{} = request} <- Vault.create_request(request_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.request_path(conn, :show, request))
      |> render("show.json", request: request)
    end
  end

  def show(conn, %{"id" => id}) do
    request = Vault.get_request!(id)
    render(conn, "show.json", request: request)
  end

  def update(conn, %{"id" => id, "request" => request_params}) do
    request = Vault.get_request!(id)

    with {:ok, %Request{} = request} <- Vault.update_request(request, request_params) do
      render(conn, "show.json", request: request)
    end
  end

  def delete(conn, %{"id" => id}) do
    request = Vault.get_request!(id)

    with {:ok, %Request{}} <- Vault.delete_request(request) do
      send_resp(conn, :no_content, "")
    end
  end
end
