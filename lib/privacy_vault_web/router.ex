defmodule PrivacyVaultWeb.Router do
  use PrivacyVaultWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]

    resources "/requests", RequestController, except: [:new, :edit]
  end

  scope "/", PrivacyVaultWeb do
    pipe_through :browser

    get "/*path", PageController, :index
  end

  # Other scopes may use custom stacks.
  # scope "/api", PrivacyVaultWeb do
  #   pipe_through :api
  # end
end
