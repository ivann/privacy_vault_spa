defmodule PrivacyVault.Repo.Migrations.CreateRequests do
  use Ecto.Migration

  def change do
    create table(:requests) do
      add :email, :string
      add :key, :string
      add :info, :string
      add :center, :string

      timestamps()
    end

  end
end
