module Id exposing (Id, toString, urlParser)

import Url.Parser exposing (Parser)



-- TYPES


type Id
    = Id Int



-- CREATE


urlParser : Parser (Id -> a) a
urlParser =
    Url.Parser.custom "ID" (\s -> Maybe.map Id (String.toInt s))



-- TRANSFORM


toString : Id -> String
toString (Id int) =
    String.fromInt int
