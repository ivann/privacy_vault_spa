module Page.NewRequest exposing (Model, Msg, init, toSession, update, view)

import Dict exposing (Dict)
import Element exposing (..)
import Element.Input as Input
import Session exposing (Session)



-- MODEL


type alias Model =
    { session : Session
    , email : String
    , key : String
    , centers : Dict String Bool
    , info : String
    }


init : Session -> ( Model, Cmd msg )
init session =
    ( { session = session
      , email = ""
      , key = "GenerateMe"
      , centers =
            Dict.fromList
                [ ( "center1", False )
                , ( "center2", False )
                , ( "center3", False )
                ]
      , info = ""
      }
    , Cmd.none
    )



-- VIEW


view : Model -> { title : String, content : Element Msg }
view model =
    { title = "New Request"
    , content =
        column []
            [ el [] (text "New Request")
            , viewForm model
            ]
    }


viewForm : Model -> Element Msg
viewForm model =
    column [ padding 16 ] <|
        Input.email
            []
            { label = Input.labelAbove [] (text "email")
            , onChange = \new -> Update { model | email = new }
            , text = model.email
            , placeholder = Nothing
            }
            :: checkboxes model
            ++ [ Input.multiline []
                    { text = model.info
                    , onChange = \new -> Update { model | info = new }
                    , label = Input.labelAbove [] (text "additional info")
                    , spellcheck = False
                    , placeholder = Nothing
                    }
               , Input.button []
                    { label = text "Submit"
                    , onPress = Just Submit
                    }
               ]


checkboxes : Model -> List (Element Msg)
checkboxes model =
    let
        makeCheckbox choice =
            Input.checkbox []
                { checked = Maybe.withDefault False (Dict.get choice model.centers)
                , onChange = \new -> Update { model | centers = Dict.insert choice new model.centers }
                , icon = Input.defaultCheckbox
                , label = Input.labelLeft [] (text choice)
                }
    in
    List.map makeCheckbox (Dict.keys model.centers)



-- UPDATE


type Msg
    = Update Model
    | Submit


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Update new ->
            ( new, Cmd.none )

        Submit ->
            ( model, Cmd.none )


toSession : Model -> Session
toSession model =
    model.session
