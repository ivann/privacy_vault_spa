module Page.Home exposing (Model, init, toSession, view)

import Element exposing (Element, el, text)
import Session exposing (Session)



-- MODEL


type alias Model =
    { session : Session }


init : Session -> ( Model, Cmd msg )
init session =
    ( { session = session }, Cmd.none )



-- VIEW


view : Model -> { title : String, content : Element msg }
view model =
    { title = "Home"
    , content = el [] (text "Home")
    }


toSession : Model -> Session
toSession model =
    model.session
