module Page.Request exposing (Model, init, toSession, view)

import Element exposing (Element, el, text)
import Id exposing (Id)
import Session exposing (Session)



-- MODEL


type alias Model =
    { session : Session
    , data : String
    }


init : Session -> Id -> ( Model, Cmd msg )
init session id =
    ( { session = session, data = "Lots of data" }, Cmd.none )



-- VIEW


view : Model -> { title : String, content : Element msg }
view model =
    { title = "Some Request"
    , content = el [] (text model.data)
    }


toSession : Model -> Session
toSession model =
    model.session
