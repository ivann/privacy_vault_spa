module Page.Requests exposing (Model, init, toSession, view)

import Element exposing (Element, el, text)
import Page.Request as Request
import Session exposing (Session)



-- MODEL


type alias Model =
    { session : Session, list : List Request.Model }


init : Session -> ( Model, Cmd msg )
init session =
    ( { session = session, list = [] }, Cmd.none )



-- VIEW


view : Model -> { title : String, content : Element msg }
view model =
    { title = "All Requests"
    , content = el [] (text "All Requests")
    }


toSession : Model -> Session
toSession model =
    model.session
