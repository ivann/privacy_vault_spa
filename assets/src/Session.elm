module Session exposing (Session, init, navKey)

import Browser.Navigation as Navigation


type Session
    = Session Navigation.Key


init : Navigation.Key -> Session
init key =
    Session key


navKey : Session -> Navigation.Key
navKey session =
    case session of
        Session key ->
            key
