module Page exposing (Page(..), view)

import Browser exposing (Document)
import Element exposing (..)
import Element.Background as Background
import Element.Font as Font
import Element.Region as Region
import Route


type Page
    = Other
    | Home
    | NewRequest
    | Request
    | Requests


view : Page -> { title : String, content : Element msg } -> Document msg
view page { title, content } =
    { title = title ++ " - Privacy Vault"
    , body =
        [ layout
            [ Background.color colors.lightGrey
            , Font.color colors.darkGrey
            ]
          <|
            column
                [ height fill
                , width fill
                , spacing 8
                ]
                [ viewHeader
                , viewContent content
                ]
        ]
    }


colors =
    { darkGrey = rgb255 54 54 54
    , lightGrey = rgb255 245 245 245
    }


scaled : Int -> Int
scaled factor =
    let
        base =
            16
    in
    if factor > 0 then
        base * 1.25 ^ toFloat (factor - 1) |> round

    else if factor == 0 then
        base

    else
        -- negative factor
        base * 1.25 ^ toFloat factor |> round


viewHeader : Element msg
viewHeader =
    column
        [ width fill
        , height <| px 200
        , Background.color colors.darkGrey
        , Background.gradient
            { angle = 2.5
            , steps =
                [ rgb255 31 25 26
                , rgb255 54 54 54
                , rgb255 70 64 63
                ]
            }
        , Font.color colors.lightGrey
        ]
        [ el
            [ height fill
            , Font.size (scaled 4)
            , Region.heading 1
            , centerY
            , padding 30
            ]
            (text "Privacy Vault")
        , row
            [ Region.navigation
            ]
            [ link [ padding 8 ] { url = Route.url Route.Home, label = text "Home" }
            , link [ padding 8 ] { url = Route.url Route.NewRequest, label = text "New" }
            , link [ padding 8 ] { url = Route.url Route.Requests, label = text "List" }
            ]
        ]


viewContent : Element msg -> Element msg
viewContent content =
    el
        [ Region.mainContent
        , padding 8
        , centerX
        ]
        content
