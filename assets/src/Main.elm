module Main exposing (main)

import Browser exposing (Document)
import Browser.Navigation as Navigation
import Element exposing (..)
import Html
import Id exposing (Id)
import Page
import Page.Blank as Blank
import Page.Home as Home
import Page.NewRequest as NewRequest
import Page.NotFound as NotFound
import Page.Request as Request
import Page.Requests as Requests
import Route exposing (Route)
import Session exposing (Session)
import Url exposing (Url)


type Model
    = Home Home.Model
    | Redirect Session
    | NotFound Session
    | NewRequest NewRequest.Model
    | Request Id Request.Model
    | Requests Requests.Model



-- MODEL


init : () -> Url.Url -> Navigation.Key -> ( Model, Cmd Msg )
init flags url navKey =
    changeRouteTo (Route.fromUrl url) (Redirect <| Session.init navKey)



-- VIEW


view : Model -> Document Msg
view model =
    case model of
        Redirect _ ->
            viewPage Page.Other (\_ -> Ignored) Blank.view

        NotFound _ ->
            viewPage Page.Other (\_ -> Ignored) NotFound.view

        Home home ->
            viewPage Page.Home (\_ -> Ignored) (Home.view home)

        NewRequest newRequest ->
            viewPage Page.NewRequest GotNewRequestMsg (NewRequest.view newRequest)

        Request id request ->
            viewPage Page.Request (\_ -> Ignored) (Request.view request)

        Requests requests ->
            viewPage Page.Requests (\_ -> Ignored) (Requests.view requests)


viewPage page toMsg config =
    let
        { title, body } =
            Page.view page config
    in
    { title = title
    , body = List.map (Html.map toMsg) body
    }



-- UPDATE


type Msg
    = Ignored
    | ChangedUrl Url
    | ChangedRoute (Maybe Route)
    | ClickedLink Browser.UrlRequest
    | GotNewRequestMsg NewRequest.Msg


toSession : Model -> Session
toSession page =
    case page of
        Redirect session ->
            session

        NotFound session ->
            session

        Home home ->
            Home.toSession home

        NewRequest newRequest ->
            NewRequest.toSession newRequest

        Request _ request ->
            Request.toSession request

        Requests requests ->
            Requests.toSession requests


changeRouteTo : Maybe Route -> Model -> ( Model, Cmd Msg )
changeRouteTo maybeRoute model =
    let
        session =
            toSession model
    in
    case maybeRoute of
        Nothing ->
            ( NotFound session, Cmd.none )

        Just Route.Home ->
            Home.init session |> updateWith Home (\_ -> Ignored) model

        Just Route.NewRequest ->
            NewRequest.init session |> updateWith NewRequest GotNewRequestMsg model

        Just (Route.Request requestId) ->
            Request.init session requestId |> updateWith (Request requestId) (\_ -> Ignored) model

        Just Route.Requests ->
            Requests.init session |> updateWith Requests (\_ -> Ignored) model


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case ( msg, model ) of
        ( Ignored, _ ) ->
            ( model, Cmd.none )

        ( ClickedLink urlRequest, _ ) ->
            case urlRequest of
                Browser.Internal url ->
                    ( model
                    , Navigation.pushUrl (Session.navKey (toSession model)) (Url.toString url)
                    )

                Browser.External href ->
                    ( model, Navigation.load href )

        ( ChangedUrl url, _ ) ->
            changeRouteTo (Route.fromUrl url) model

        ( ChangedRoute route, _ ) ->
            changeRouteTo route model

        ( GotNewRequestMsg subMsg, NewRequest newRequest ) ->
            NewRequest.update subMsg newRequest
                |> updateWith NewRequest GotNewRequestMsg model

        ( _, _ ) ->
            ( model, Cmd.none )


updateWith : (subModel -> Model) -> (subMsg -> Msg) -> Model -> ( subModel, Cmd subMsg ) -> ( Model, Cmd Msg )
updateWith toModel toMsg model ( subModel, subCmd ) =
    ( toModel subModel
    , Cmd.map toMsg subCmd
    )



-- SUBSCRIPTION


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-- MAIN


main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlChange = ChangedUrl
        , onUrlRequest = ClickedLink
        }
