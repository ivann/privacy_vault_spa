module Route exposing (Route(..), fromUrl, href, replaceUrl, url)

import Browser.Navigation as Navigation
import Html exposing (Attribute)
import Html.Attributes as Attr
import Id exposing (Id)
import Url exposing (Url)
import Url.Parser as Parser exposing ((</>), Parser, map, oneOf, s)


type Route
    = Home
    | NewRequest
    | Request Id
    | Requests


routeParser : Parser (Route -> a) a
routeParser =
    oneOf
        [ map Home Parser.top
        , map NewRequest (s "request" </> s "new")
        , map Request (s "request" </> Id.urlParser)
        , map Requests (s "request")
        ]


href : Route -> Attribute msg
href targetRoute =
    Attr.href (routeToString targetRoute)


url : Route -> String
url =
    routeToString


replaceUrl : Navigation.Key -> Route -> Cmd msg
replaceUrl key route =
    Navigation.replaceUrl key (routeToString route)


fromUrl : Url -> Maybe Route
fromUrl =
    Parser.parse routeParser


routeToString : Route -> String
routeToString page =
    let
        pieces =
            case page of
                Home ->
                    []

                NewRequest ->
                    [ "request", "new" ]

                Request id ->
                    [ "request", Id.toString id ]

                Requests ->
                    [ "request" ]
    in
    "/" ++ String.join "/" pieces
