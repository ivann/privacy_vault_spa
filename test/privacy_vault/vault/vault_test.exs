defmodule PrivacyVault.VaultTest do
  use PrivacyVault.DataCase

  alias PrivacyVault.Vault

  describe "requests" do
    alias PrivacyVault.Vault.Request

    @valid_attrs %{center: "some center", email: "some email", info: "some info", key: "some key"}
    @update_attrs %{center: "some updated center", email: "some updated email", info: "some updated info", key: "some updated key"}
    @invalid_attrs %{center: nil, email: nil, info: nil, key: nil}

    def request_fixture(attrs \\ %{}) do
      {:ok, request} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Vault.create_request()

      request
    end

    test "list_requests/0 returns all requests" do
      request = request_fixture()
      assert Vault.list_requests() == [request]
    end

    test "get_request!/1 returns the request with given id" do
      request = request_fixture()
      assert Vault.get_request!(request.id) == request
    end

    test "create_request/1 with valid data creates a request" do
      assert {:ok, %Request{} = request} = Vault.create_request(@valid_attrs)
      assert request.center == "some center"
      assert request.email == "some email"
      assert request.info == "some info"
      assert request.key == "some key"
    end

    test "create_request/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Vault.create_request(@invalid_attrs)
    end

    test "update_request/2 with valid data updates the request" do
      request = request_fixture()
      assert {:ok, %Request{} = request} = Vault.update_request(request, @update_attrs)
      assert request.center == "some updated center"
      assert request.email == "some updated email"
      assert request.info == "some updated info"
      assert request.key == "some updated key"
    end

    test "update_request/2 with invalid data returns error changeset" do
      request = request_fixture()
      assert {:error, %Ecto.Changeset{}} = Vault.update_request(request, @invalid_attrs)
      assert request == Vault.get_request!(request.id)
    end

    test "delete_request/1 deletes the request" do
      request = request_fixture()
      assert {:ok, %Request{}} = Vault.delete_request(request)
      assert_raise Ecto.NoResultsError, fn -> Vault.get_request!(request.id) end
    end

    test "change_request/1 returns a request changeset" do
      request = request_fixture()
      assert %Ecto.Changeset{} = Vault.change_request(request)
    end
  end
end
